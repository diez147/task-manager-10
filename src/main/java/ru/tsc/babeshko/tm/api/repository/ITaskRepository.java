package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    List<Task> findAll();

    Task create(String name, String description);

    void remove(Task task);

    Task add(Task task);

    void clear();

}
