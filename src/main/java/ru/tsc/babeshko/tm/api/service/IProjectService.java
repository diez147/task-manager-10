package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void remove(Project project);

    void clear();

}
