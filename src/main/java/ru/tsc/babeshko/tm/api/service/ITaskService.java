package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    void remove(Task task);

    void clear();

}
