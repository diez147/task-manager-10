package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
