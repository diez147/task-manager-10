package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    List<Project> findAll();

    Project create(String name, String description);

    void remove(Project project);

    Project add(Project project);

    void clear();

}
