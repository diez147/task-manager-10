package ru.tsc.babeshko.tm.controller;

import ru.tsc.babeshko.tm.api.controller.ITaskController;
import ru.tsc.babeshko.tm.api.service.ITaskService;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTaskList() {
        final List<Task> tasks = taskService.findAll();
        System.out.println("[TASK LIST]");
        for (final Task task: tasks){
            if (task == null) continue;
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
