package ru.tsc.babeshko.tm.repository;

import ru.tsc.babeshko.tm.api.repository.ICommandRepository;
import ru.tsc.babeshko.tm.constant.ArgumentConst;
import ru.tsc.babeshko.tm.constant.TerminalConst;
import ru.tsc.babeshko.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show program version."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show list of terminal commands."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show command list."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show argument list."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Clear task list."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Clear project list."
    );

    private final Command[] terminalCommands = new Command[]{
            ABOUT, INFO, VERSION, HELP,
            COMMANDS, ARGUMENTS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
